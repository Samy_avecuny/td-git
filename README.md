# Mon Projet

## Description
C'est un projet cool qui fait des trucs géniaux.

## Comment l'utiliser

1. Clonez ce dépôt sur votre machine locale :
git clone https://gitlab.com/Samy_avecuny/td-git

2. Exécutez le fichier principal :
python main.py

3. Profitez des trucs géniaux !

## Contributions
Les contributions sont les bienvenues ! N'hésitez pas à ouvrir une issue ou à soumettre une demande de fusion.

## Auteurs
Samy

## Licence
Ce projet est sous licence MIT - voir le fichier [LICENSE](LICENSE) pour plus de détails.
